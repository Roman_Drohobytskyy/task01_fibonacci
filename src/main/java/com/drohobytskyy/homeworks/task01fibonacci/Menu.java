package com.drohobytskyy.homeworks.task01fibonacci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Roman Drohobytskyy
 * @version 1.0
 * This class is used as menu for first homework tasks.
 */
public class Menu {

    private static BufferedReader bufferedReader;

    /**
     * Entry point of the application.
     */
    public static void main(final String[] args){
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputText = "";

        System.out.println("Welcome to Homework task01_Fibonacci!");
        for (;;) {
            System.out.println("--------------------------------------------");
            System.out.println("Please choose an appropriate item from menu:");
            System.out.println("Range task -> range");
            System.out.println("Fibonacci task -> fibonacci");
            System.out.println("Fibonacci in the recursion -> recursion");
            System.out.println("If you want to exit from application -> exit");
            System.out.println("...........");

            try {
                inputText = bufferedReader.readLine().toLowerCase();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (inputText.equals("range")) {
                try {
                    task01_Fibonacci.resolveRangeTask();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (inputText.equals("fibonacci")) {
                try {
                    task01_Fibonacci.resolveFibonacciTask();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (inputText.equals("recursion")) {
                try {
                    task01_Fibonacci.resolveFibonacciInRecursion();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (inputText.equals("exit")) {
                break;
            } else {
                System.out.println("Incorrect input! Please try again.");
            }
        }
        System.out.println("See you next time! Have a nice day!");

    }
}
