package com.drohobytskyy.homeworks.task01fibonacci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * @author Roman Drohobytskyy
 * @version 1.0
 * This class resolves tasks from first homework.
 */

public class task01_Fibonacci {

    private static BufferedReader bufferedReader = new BufferedReader(
                                    new InputStreamReader(System.in));

    /**
     * Method finds Fibonacci number using recursion.
     * @throws IOException
     */
    public static void resolveFibonacciInRecursion() throws IOException {
        System.out.println(findFibonacciInRecursion(inputPositiveInteger(
                                "N (positive integer)")));
    }

    private static int findFibonacciInRecursion(final int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return findFibonacciInRecursion(n - 1)
                + findFibonacciInRecursion(n - 2);
    }

    /**
     * Method resolve first part of the homework task.
     * It prints all odd numbers ascending and all even numbers descending from the range.
     * Also method prints sum of odd and even numbers.
     * @throws IOException
     */
    public static void resolveRangeTask() throws IOException {
        int startOfInterval;
        int endOfInterval;
        startOfInterval = inputInteger("start (integer)");
        endOfInterval = inputInteger("end (integer)");
        printOddNumbersAscending(startOfInterval, endOfInterval);
        printEvenNumbersDescending(startOfInterval, endOfInterval);
        printSumOfOddAndEvenNumbers(startOfInterval, endOfInterval);
    }

    private static void printOddNumbersAscending(
            final int startOfInterval, final int endOfInterval) {
        System.out.print("Odd numbers from interval " + "[" + startOfInterval
                + ";" + endOfInterval + "] are: ");
        for (int i = startOfInterval; i <= endOfInterval; i++) {
            if (i % 2 != 0) {
                if (i < (endOfInterval - 1)) {
                    System.out.print(i + ", ");
                } else {
                    System.out.println(i);
                }
            }
        }
    }

    private static void printEvenNumbersDescending(
            final int startOfInterval, final int endOfInterval) {
        System.out.print("Even numbers from interval " + "["
                + startOfInterval + ";" + endOfInterval + "] are: ");
        for (int i = endOfInterval; i >= startOfInterval; i--) {
            if (i % 2 == 0) {
                if (i <= startOfInterval + 1) {
                    System.out.println(i);
                } else {
                    System.out.print(i + ", ");
                }
            }
        }
    }

    private static void printSumOfOddAndEvenNumbers(
            final int startOfInterval, final int endOfInterval) {
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;
        for (int i = startOfInterval; i <= endOfInterval; i++) {
            if (i % 2 == 0) {
                sumOfEvenNumbers += i;
            } else {
                sumOfOddNumbers += i;
            }
        }
        System.out.println("Sum of odd numbers = " + sumOfOddNumbers
                + "; sum of even numbers = " + sumOfEvenNumbers);
    }

    /**
     * Method resolve second part of the homework task.
     * It finds N Fibonacci numbers in the loop, prints all of them.
     * Also method prints max odd number and max even number from the set,
     * percentage of odd and even numbers, sum of odd and even numbers.
     * @throws IOException
     */
    public static void resolveFibonacciTask() throws IOException {
        int n;
        int[] fibonacciNumbersArray;
        n = inputPositiveInteger("N (positive integer)");
        fibonacciNumbersArray = findSetOfFibonacci(n);
        printArray(fibonacciNumbersArray);
        printMaxOddAndEvenNumber(fibonacciNumbersArray);
        printPercentageOfOddAndEvenNumbers(fibonacciNumbersArray);
        printSumOfOddAndEvenNumbersFromArray(fibonacciNumbersArray);
    }

    private static int[] findSetOfFibonacci(final int n) {
        int[] fibonacciNumbers = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            if (i == 0) {
                fibonacciNumbers[i] = 0;
            } else if (i == 1) {
                fibonacciNumbers[i] = 1;
            } else {
                fibonacciNumbers[i] = fibonacciNumbers[i - 2]
                                    + fibonacciNumbers[i - 1];
            }
        }
        return fibonacciNumbers;
    }

    private static void printArray(final int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                System.out.println(array[i]);
            } else {
                System.out.print(array[i] + ", ");
            }
        }
    }

    private static void printMaxOddAndEvenNumber(final int[] arrayOfNumbers) {
        int maxOdd = 0;
        int maxEven = 0;
        for (int el : arrayOfNumbers) {
            if (el % 2 == 0) {
                maxEven = el;
            } else {
                maxOdd = el;
            }
        }
        System.out.println("Maximum odd number: " + maxOdd);
        System.out.println("Maximum even number: " + maxEven);
    }

    private static void printPercentageOfOddAndEvenNumbers(
                                final int[] arrayOfNumbers) {
        int percentOfOddNumbers;
        int percentOfEvenNumbers;
        int countOfOddNumbers = 0;
        int countOfEvenNumbers = 0;
        final int maximumPercent100 = 100;
        for (int el : arrayOfNumbers) {
            if (el % 2 == 0) {
                countOfEvenNumbers++;
            } else {
                countOfOddNumbers++;
            }
        }
        percentOfOddNumbers = countOfOddNumbers * maximumPercent100
                        / (countOfEvenNumbers + countOfOddNumbers);
        percentOfEvenNumbers = maximumPercent100 - percentOfOddNumbers;
        System.out.println("Percent of odd numbers = "
                        + percentOfOddNumbers + "%");
        System.out.println("Percent of even numbers = "
                        + percentOfEvenNumbers + "%");
    }

    private static void printSumOfOddAndEvenNumbersFromArray(
            final int[] arrayOfNumbers) {
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;
        for (int el : arrayOfNumbers) {
            if (el % 2 == 0) {
                sumOfEvenNumbers += el;
            } else {
                sumOfOddNumbers += el;
            }
        }
        System.out.println("Sum of odd numbers = " + sumOfOddNumbers);
        System.out.println("Sum of even numbers = " + sumOfEvenNumbers);
    }


    private static boolean isInteger(final String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    private static int inputInteger(final String variableDescription)
                                        throws IOException {
        String stringFromBufferedReader;
        boolean isIntegerFounded = false;
        do {
            System.out.println("Please, enter value " + variableDescription
                                                                    + " : ");
            stringFromBufferedReader = bufferedReader.readLine();
            if (!isInteger(stringFromBufferedReader)) {
                System.out.println("Incorrect value! Please, try again.");
            } else {
                isIntegerFounded = !isIntegerFounded;
            }
        } while (!isIntegerFounded);
        return Integer.parseInt(stringFromBufferedReader);
    }

    private static int inputPositiveInteger(final String variableDescription)
                                                    throws IOException {
        int number;
        boolean isPositiveIntegerFounded = false;
        do {
            number = inputInteger(variableDescription);
            if (number < 0) {
                System.out.println("Incorrect value! Please, try again.");
            } else {
                isPositiveIntegerFounded = !isPositiveIntegerFounded;
            }
        } while (!isPositiveIntegerFounded);
        return number;
    }


}